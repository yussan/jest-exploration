import React from "react";

import { likeNews } from "../../redux/actions/news";

const LikeButton = props => {
  return (
    <button onClick={() => props.dispatch(likeNews("list", props.postKey))}>{`${
      props.isLiked ? "Unlike" : "Like"
    } this news`}</button>
  );
};

export default LikeButton;
