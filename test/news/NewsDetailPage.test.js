import React from "react"
import NewsDetailPage from "../../src/pages/NewsDetail"
import renderer from "react-test-renderer"

// about snapshot testing : https://jestjs.io/docs/en/snapshot-testing
test("Snapshot Test: Detail Page Test", () => {
  const PageComponent = renderer.create(<NewsDetailPage />)
  let PageComponentTree = PageComponent.toJSON()

  // will check current component with  new snapshot file
  expect(PageComponentTree).toMatchInlineSnapshot(`
  Array [
    <strong>
      This is Post Detail
    </strong>,
    <br />,
  ]
  `)
})
