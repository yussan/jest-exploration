import React from "react"
import {cleanup, fireEvent, render} from "@testing-library/react"
import NewsDetailPage from "../../src/pages/NewsDetail"

// react components
import LikeButton from "../../src/components/buttons/LikeButton"

// automatically unmount and cleanup DOM after test finished
afterEach(cleanup)

